/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author adama
 */
public class BankTest {

    private static EntityManagerFactory emf;
    private static final Logger LOG = Logger.getLogger(BankTest.class.getName());
    private EntityManager em;
    private EntityTransaction tx;

    //this we want to do once at the class level
    @BeforeClass
    public static void beforeClassTestFixture() {
        emf = Persistence.createEntityManagerFactory("itmd4515PU");
    }

    @AfterClass
    public static void afterClassTestFixture() {
        emf.close();
    }

    @Before
    public void beforeEachTest() {
        em = emf.createEntityManager();
        tx = em.getTransaction();

        //create an instance for a SEED object
        //this is going to fire before each test case
        //we want this for testing purposes
//        Bank seed = new Bank(
//                "SEED",
//                "12345 North Brook IL",
//                new GregorianCalendar(1979, 45, 3).getTime());
//        //get the transaction started for our seed object
//        tx.begin();
//        em.persist(seed);
//        tx.commit();
    }

    @After
    public void afterEachTest() {
        //we want to remove the seed data before each test case
//        Bank seed = em
//                .createNamedQuery("Bank.findByName", Bank.class)
//                .setParameter("name", "SEED")
//                .getSingleResult();
//
//        tx.begin();
//        em.remove(seed);
//        tx.commit();

        if (em != null) {
            em.close();
        }
    }
//    
//    @Test
//    public void verifySeedData(){
//        List<Bank> seeds = em
//                .createNamedQuery("Bank.findByName", Bank.class)
//                .setParameter("name", "SEED")
//                .getResultList();
//
//        assertEquals(seeds.size(),1);
//        assertEquals("SEED", seeds.get(0).getName());
//    }

    //***Sunny Day Test of our persist working properly****************
    @Test
    public void persistNewBankSuccess() {
        //create an instance for our bank object
        Bank bank = new Bank(
                "Centrust",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());

        tx.begin();
        //check of our id is null before we persist
        assertNull("ID should not be null before persist()", bank.getId());
        //persist our new bank to the table
        em.persist(bank);
        //check of our id is null before we persist
        assertNull("ID should still be null after persist()", bank.getId());
        tx.commit();
        assertNotNull("ID should not be null after commit()", bank.getId());
        //check that our id should be greater than zero
        assertTrue("ID should be greater than zer0", bank.getId() > 0);

    }

    //***Rainy Day Test of our persist not working properly****************
    @Test
    public void persistNewBankFailure() {
        //create an instance for our bank object
        Bank bank = new Bank(
                "SEED",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());

        tx.begin();
        //check of our id is null before we persist
        em.persist(bank);
        //check of our id is null before we persist
        assertNull("ID should still be null after persist()", bank.getId());
        tx.commit();
    }

    //***Sunny Day Test of Update working properly****************
    @Test
    public void updateNewBankSuccess() {
        //create an instance for our bank object
        Bank bank = new Bank(
                "Centrust2",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());
        tx.begin();
        //check of our id is null before we persist
        // insert new record into database.
        em.persist(bank);
        //check of our id is null before we persist
        assertNull("ID should still be null after persist()", bank.getId());
        tx.commit();

        tx.begin();
        //persist our new bank to the table
        em.persist(bank);
        //update name on bank record.
        bank.setName("Alex_Bank");
        tx.commit();

        List<Bank> seeds = em
                .createNamedQuery("Bank.findByName", Bank.class)
                .setParameter("name", "Alex_Bank")
                .getResultList();

        assertEquals(seeds.size(), 1);
        assertEquals("Alex_Bank", seeds.get(0).getName());

    }

    @Test(expected = RollbackException.class)
    public void updateExistingBankFailure() {
        //create an instance for our bank object
        Bank bank = new Bank(
                "UpdateBankFail",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());
        tx.begin();
        //check of our id is null before we persist
        // insert new record into database.
        em.persist(bank);
        //check of our id is null before we persist
        assertNull("ID should still be null after persist()", bank.getId());
        tx.commit();
        
        //create a second 
         Bank bank2 = new Bank(
                "UpdateBankFail2",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());
        tx.begin();
        //check of our id is null before we persist
        // insert new record into database.
        em.persist(bank2);
        //check of our id is null before we persist
        assertNull("ID should still be null after persist()", bank2.getId());
        tx.commit();

        tx.begin();
        //persist our new bank to the table
        em.persist(bank);
        //update name on bank record.
        bank.setName("UpdateBankFail2");
        tx.commit();
    }

    //***Sunny Day Test of Read working properly****************
    @Test
    public void readNewBankSuccess() {
        //create an instance for our bank object
        Bank bank = new Bank(
                "Centrust2",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());

        Bank findBank = em.find(Bank.class, 3L);
        if (findBank != null) {
            LOG.info("Bank Name: " + findBank.getName());
        }
    }

    @Test
    public void readNewBankFailure() {
        //create an instance for our bank object
        Bank bank = new Bank(
                "Centrust2",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());

        Bank findBank = em.find(Bank.class, 6L);
        if (findBank == null) {
            LOG.info("Bank Name Not Found: ");
        }
    }

    @Test
    public void deleteBankSuccess() {
        //create an instance for our bank object
        Bank bank = new Bank(
                "Bank_To_Delete",
                "12345 North Brook IL",
                new GregorianCalendar(1979, 45, 3).getTime());

        tx.begin();
        //check of our id is null before we persist
        assertNull("ID should not be null before persist()", bank.getId());
        //persist our new bank to the table
        em.persist(bank);
        //check of our id is null before we persist
        assertNull("ID should still be null after persist()", bank.getId());
        tx.commit();
        tx.begin();
        em.remove(bank);
        tx.commit();
        assertNotNull("Removed the Centrust bank element", bank);
    }

}
