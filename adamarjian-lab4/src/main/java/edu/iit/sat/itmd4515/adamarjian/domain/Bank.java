/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author adama
 */
@Entity
@Table(name = "bank")
//create a named query to help us find our bank object
@NamedQuery(
        name = "Bank.findByName",
        query = "select b from Bank b where b.bankName = :name")
public class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    //this is our bankname
     @Column(name = "bank_name",unique=true)
    private String bankName;
    @Column(name = "bank_location")
    private String location;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_joined")
    private Date dateJoined;

    public Bank() {
    }

    public Bank(String bankName, String location, Date dateJoined) {
        this.bankName = bankName;
        this.location = location;
        this.dateJoined = dateJoined;
    }
    
    

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return bankName;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.bankName = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Bank{" + "id=" + id + ", bankName=" + bankName + ", location=" + location + ", dateJoined=" + dateJoined + '}';
    }

}
